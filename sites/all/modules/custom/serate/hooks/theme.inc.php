<?php
function serate_theme(){
  $theme_path = path_to_theme();
  $theme = array();
  $theme['serate-home'] = array('template' => 'serate-home', 'path'=>$theme_path.'/templates');
  $theme['giorno'] = array('template' => 'giorno', 'path'=>$theme_path.'/templates');
  $theme['serate-dettaglio'] = array('template' => 'serate-dettaglio', 'path'=>$theme_path.'/templates');
  $theme['serate-prenota'] = array('template' => 'prenota', 'path'=>$theme_path.'/templates');
  return $theme;
}

<?php
function serate_menu(){
  $menu = array();
  $menu['serate'] = array(
    'title' => 'Serate',
    'access callback' => TRUE,
    'page callback' => 'serate_get_page',
    'page arguments'=> array('serate-home'),
  );

  $menu['serate/confirm/%'] = array(
    'title' => 'Serate',
    'access callback' => TRUE,
    'page callback' => 'serate_confirm',
    'page arguments'=> array(2),
  );
  $menu['serate/%'] = array(
    'title' => 'Serate',
    'access callback' => TRUE,
    'page callback' => 'serate_get_page',
    'page arguments'=> array('giorno', 1),
  );
  $menu['serate/%/%'] = array(
    'title' => 'Serata',
    'access callback' => TRUE,
    'page callback' => 'serate_get_page',
    'page arguments'=> array('dettaglio',1,2),
  );
  return $menu;
}
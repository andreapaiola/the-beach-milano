<?php
function prenota_serata_form(){
  global $base_url;
  if(isset($_GET['serata']) && is_numeric($_GET['serata'])){
    $node = node_load($_GET['serata']);
    if($node == false){
      drupal_set_message('Indirizzo non valido. Per vedere l\'elenco delle serate <a href="'.$base_url.'/serate">clicca qui</a>', 'warning');
      return array();
    }
  } else {
    drupal_set_message('Indirizzo non valido. Per vedere l\'elenco delle serate <a href="'.$base_url.'/serate">clicca qui</a>', 'warning');
    return array();
  }
  $form = array(
    '#method' => 'post',
  );

  $form['id_serata'] = array(
    '#type' => 'hidden',
    '#default_value' => $node->nid,
    '#required' => TRUE,
  );

  $form['nome'] = array(
    '#type' => 'textfield',
    '#title' => 'Nome',
    '#default_value' => (isset($_POST['nome'])) ? $_POST['nome'] : '',
    '#required' => TRUE,
    '#attributes' => array('placeholder'=>'Nome *'),
  );

  $form['cognome'] = array(
    '#type' => 'textfield',
    '#title' => 'Cognome',
    '#default_value' => (isset($_POST['cognome'])) ? $_POST['cognome'] : '',
    '#required' => TRUE,
    '#attributes' => array('placeholder'=>'Cognome *'),
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => 'Email',
    '#default_value' => (isset($_POST['email'])) ? $_POST['email'] : '',
    '#required' => TRUE,
    '#attributes' => array('placeholder'=>'Email *'),
  );

  $form['cellulare'] = array(
    '#type' => 'textfield',
    '#title' => 'Cellulare',
    '#default_value' => (isset($_POST['cellulare'])) ? $_POST['cellulare'] : '',
    '#required' => TRUE,
    '#attributes' => array('placeholder'=>'Cellulare *'),
  );

  $form['numero_persone'] = array(
    '#type' => 'textfield',
    '#title' => 'Numero persone',
    '#default_value' => (isset($_POST['numero_persone'])) ? $_POST['numero_persone'] : '',
    '#required' => TRUE,
    '#attributes' => array('placeholder'=>'Numero persone *'),
  );

	$form['privacy'] = array(
		'#type' => 'checkboxes',
		'#title' => 'Consenso Privacy Policy',
		'#options' => array(1 => 'Acconsento al trattamento dei miei dati e dichiaro di aver preso visione della <a href="/informativa" target="_blank">Privacy Policy</a>'),
		//    '#prefix' => '<p class="consenso">CONSENSO EX ART 13 DEL DLGS N° 196/03 <sup class="form-required" title="Questo campo è obbligatorio.">*</sup></p>',
		'#required' => TRUE,
		'#attributes' => array(),
	);

	$form['marketing'] = array(
		'#type' => 'checkboxes',
		'#title' => 'Consenso Marketing',
		'#options' => array(1 => 'Acconsento al trattamento dei miei dati personali per finalità di marketing e per ricevere periodicamente informazioni relative alle vostre iniziative promozionali e commerciali inviate per e-mail, sms o WhatsApp'),
		'#required' => false,
		'#attributes' => array(),
  );

  $form['invia'] = array(
    '#type' => 'submit',
    '#value' => 'Invia',
    '#attributes' => array(),
    '#weight' => 200,
  );
  return $form;
}

function  prenota_serata_form_validate(&$form, &$form_state){
  $values = $form_state['values'];

  //$values['nome'];
  //
  if($values['cellulare'] != '' && !preg_match('/^([\+]?)([0-9\s]{5,20})$/', $values['cellulare'])){
    form_set_error('cellulare', "Il numero di telefono inserito non può essere accettato.");
  }
  if(!valid_email_address($values['email'])){
    form_set_error('email', "L'indirizzo Email inserito non risulta valido.");
  }

    //Hijack our errors so they don't display normally.
    $errors = form_get_errors();
    //Only rebuild form if there are actually errors that we need to display
    if( !empty($errors) ) {
        $form_state['errors'] = $errors;
        $form['errors']= array(
            '#type' => 'item',
            '#title' => 'Errori',
            '#markup' => '<p class="errors">'.implode('<br>',$errors).'</p>',
            '#weight' => -1
        );
        if( isset($errors['privacy']) && !empty($errors['privacy']))
        {
            $form['privacy']['#attributes'] = array('class'=>array('error'));
        }
        $form_state['rebuild'] = TRUE;
        //dsm($errors);
        //dsm($form);
        //dsm($form_state);
    }
    // Remove all error messages
    drupal_get_messages('error');
}

function  prenota_serata_form_submit($form, &$form_state){
  $values = $form_state['values'];
  $token = prenota_serata_get_token($values);
  $insert_values = array();
  if(trim($values['cellulare']) == ''){
    $insert_values = array(
      'nid'=>$values['id_serata'],
      'nome'=>$values['nome'],
      'cognome'=>$values['cognome'],
      'email'=>$values['email'],
      'numero_persone'=>$values['numero_persone'],
      'privacy'=>$values['privacy'],
	  'marketing'=>$values['marketing'],
      'token' => $token,
    );
  } else {
    $insert_values = array(
      'nid'=>$values['id_serata'],
      'nome'=>$values['nome'],
      'cognome'=>$values['cognome'],
      'email'=>$values['email'],
      'cellulare'=>$values['cellulare'],
      'numero_persone'=>$values['numero_persone'],
      'privacy'=>$values['privacy'],
	  'marketing'=>$values['marketing'],
      'token' => $token,
    );
  }
  if(isset($_POST['fbdata'])){
    $insert_values['fb_data'] = serialize($_POST['fbdata']);
  }
  $query = db_insert('the_beach_prenotazioni_serata');
  $query->fields($insert_values);
  $id = $query->execute();
  $node = node_load($values['id_serata']);
  $data = format_date(strtotime($node->field_data[LANGUAGE_NONE][0]['value']),'custom','l j F');
  $to = 'thebeachmilano.com@gmail.com'; //info@thebeachmilano.com
  $subject = 'TheBeach - Iscrizione in lista ['.$id.'] - ['.$data.'] - ['.$node->title.']';
  $body = prenota_serata_get_body_mail($values, $data, $node->title);
  $body .= prenota_serata_get_token_link($token);
  $headers = prenota_serata_get_header_mail(true);
  mail($to, $subject, $body, $headers);

  drupal_set_message('La tua richiesta è stata inviata allo staff, ti risponderemo al più presto.');
}

function prenota_serata_get_body_mail($values, $data, $title){
  $cellulare = ($values['cellulare'] != '') ? $values['cellulare'] : 'non fornito';
  return 'Iscrizione alla lista<br /><br />
Data e ora della richiesta: '.date('d-m-Y H:i:s').'<br />
Nome: '.$values['nome'].'<br />
Cognome: '.$values['cognome'].'<br />
Cellulare: '.$cellulare.'<br />
E-mail: '.$values['email'].'<br /><br />
Numero di persone: '.$values['numero_persone'].'<br />
Serata: '.$data.' - '.$title.'<br />
Consenso Privacy Policy: '.($values['privacy'][1] == 1 ? 'SI' : 'NO').'<br />
Consenso Marketing tramite e-mail, sms o WhatsApp: '.($values['marketing'][1] == 1 ? 'SI' : 'NO');
}

function prenota_serata_get_token($values){
  $str = $values['nome'].$values['cognome'].$values['email'].time();
  return sha1($str);
}

function prenota_serata_get_token_link($token){
  global $base_url;
  return '<br /><br /><a href="'.$base_url.'/serate/confirm/'.$token.'">Per inviare la mail di conferma clicca qui</a>';
}
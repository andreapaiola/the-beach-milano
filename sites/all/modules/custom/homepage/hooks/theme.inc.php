<?php
function homepage_theme(){
  $theme_path = path_to_theme();
  return array('home-page' => array('template' => 'home-page', 'path'=>$theme_path.'/templates'));
}

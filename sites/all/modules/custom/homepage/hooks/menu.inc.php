<?php
function homepage_menu(){
  $menu = array();
  $menu['homepage'] = array(
    'title' => 'Home',
    'access callback' => TRUE,
    'page callback' => 'homepage_get_page',
  );
  return $menu;
}
<?php

function svuota_cache_form_alter(&$form, &$form_state, $form_id) {

	if ($form_id == 'system_performance_settings'){

		$form['flush_all_cache'] = array(
			'#type' => 'fieldset',
			'#title'=> 'Svuota tutte le cache',
			'#description' 	=> '<p style="margin-bottom:8px">A differenza della seconda opzione questa permette la cancellazione TOTALE delle cache. (Uso della funzione drupal_flush_all_caches)</p>',

			'flush' => array(
				'#type' 	=> 'submit',
				'#value' 	=>	'Svuota!',
				'#submit' 	=> array('svuota_cache_flush_all_cache'),
			),
			'#weight' 		=> -1
		);
	}
}

function svuota_cache_flush_all_cache(){
	drupal_flush_all_caches();
	drupal_set_message('Cancellazione sicura di tutti i cache effettuata!');
}

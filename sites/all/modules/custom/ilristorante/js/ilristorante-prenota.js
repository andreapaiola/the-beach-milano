jQuery(document).ready(function($){
    $("#prenota-tavolo-form select").chosen({
        disable_search: true,
        inherit_select_classes: true
    });

    $(window).smartresize(function() {
        $("#prenota-tavolo-form select").chosen("destroy").chosen({
            disable_search: true,
            inherit_select_classes: true
        });
    });

});
<?php
function ilristorante_theme(){
  $theme_path = path_to_theme();
  $theme = array();
  $theme['ilristorante-home'] = array('template' => 'il-ristorante-home', 'path'=>$theme_path.'/templates');
  $theme['ilristorante-dettaglio'] = array('template' => 'il-ristorante-dettaglio', 'path'=>$theme_path.'/templates');
  $theme['ilristorante-prenota'] = array('template' => 'prenota', 'path'=>$theme_path.'/templates');
  return $theme;
}

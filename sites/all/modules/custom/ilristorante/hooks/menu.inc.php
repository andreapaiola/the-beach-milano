<?php
function ilristorante_menu(){
  $menu = array();
  $menu['ristorante'] = array(
    'title' => 'Il ristorante',
    'access callback' => TRUE,
    'page callback' => 'ilristorante_get_page',
    'page arguments' => array('ilristorante-home'),
  );
  $menu['ristorante/%'] = array(
    'title' => 'Il ristorante',
    'access callback' => TRUE,
    'page callback' => 'ilristorante_get_page',
    'page arguments' => array('dettaglio', 1),
  );
  return $menu;
}
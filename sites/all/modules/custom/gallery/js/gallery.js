jQuery(document).ready(function($){
    $('.slider').slider();
    if( $('body').hasClass('page-gallery-video') )
    {
        $('.new-gallery').on('click','a',function(event){
            event.preventDefault();
            $('.overlay').remove();
            var overlayHTML = '<div class="overlay"><div class="overlay-content" style="margin-top:'+parseInt($(window).height()/2-240,10)+'px">';
            overlayHTML += '<a href="#close" class="close">X</a>';
            overlayHTML += '<iframe width="853" height="480" src="https://www.youtube.com/embed/'+$(this).attr('href').replace('https://www.youtube.com/watch?v=','')+'?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>';
            overlayHTML += '</div></div>';

            $('body').append(overlayHTML);

            $('.overlay a[href="#close"]').click(function(event){
                event.preventDefault();
                $('.overlay').remove();
            });
            $('.overlay').click(function(event){
                event.preventDefault();
                $('.overlay').remove();
            });
        });
        //$('.box-bottom-gallery .custom-box-content a').first().trigger('click');

    }

    $('select#cambia-categoria').change(function(event){
        event.preventDefault();
        var link = $('.load-more');
        var callurl = Drupal.settings.gallery.baseUrl+Drupal.settings.basePath+'/gallery/foto?categoria='+$(this).val();
        $.getJSON( callurl, function( data ) {
            //console.log(data);
            var items = '';
            $.each( data.dati.nodi, function( key, val ) {
                items = items+'<div class="item">'+val+'</div>';
            });

            $( '.new-gallery-content').html(items);
            if( typeof data.dati.link_load_more!="undefined" && data.dati.link_load_more!='' )
            {
                link.attr('href',data.dati.link_load_more);
                link.show();
            }
            else
            {
                link.hide();
            }
        });
    });

    $('.load-more').click(function(event){
        event.preventDefault();
        var link = $(this);
        var categoria = ($('select#cambia-categoria').val() != '') ? $('select#cambia-categoria').val() : 0;
        var callurl = $(this).attr('href')+'&categoria='+categoria;
        $.getJSON( callurl, function( data ) {
            //console.log(data);
            var items = '';
            $.each( data.dati.nodi, function( key, val ) {
                items = items+'<div class="item">'+val+'</div>';
            });
            $( '.new-gallery-content').append(items);
            if( typeof data.dati.link_load_more!="undefined" && data.dati.link_load_more!='' )
            {
                link.attr('href',data.dati.link_load_more);
            }
            else
            {
                link.hide();
            }
        });
    });

    $("#filtra-per-categoria select").chosen({
        disable_search: true,
        inherit_select_classes: true
    });

    $(window).smartresize(function() {
        $("#filtra-per-categoria select").chosen("destroy").chosen({
            disable_search: true,
            inherit_select_classes: true
        });
    });

});
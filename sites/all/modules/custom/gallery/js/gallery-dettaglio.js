var jQueryTemp = jQuery.noConflict(true);
var jQueryOriginal = jQuery || jQueryTemp;
window.jQuery = window.$ = jQueryTemp;
(function() {
    Galleria.loadTheme('../sites/all/libraries/galleria/themes/classic/galleria.classic.js');

    if( window.location.hash && window.location.hash.indexOf('#item-')===0 )
    {
        //console.log(window.location.hash.replace('#item-',''));
        Galleria.run('.galleria', {
            show: parseInt(window.location.hash.replace('#item-',''),10)
        });
    }
    else {
        Galleria.run('.galleria');
    }

}());
//$ = jQuery = jQueryOriginal;
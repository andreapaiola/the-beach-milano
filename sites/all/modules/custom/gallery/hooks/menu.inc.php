<?php
function gallery_menu(){
  $menu = array();
  $menu['gallery'] = array(
    'title' => 'Galleria - foto',
    'access callback' => TRUE,
    'page callback' => 'gallery_get_page',
    'page arguments' => array('gallery-home', 'foto'),
  );
  $menu['gallery/foto'] = array(
    'title' => 'Galleria - foto',
    'access callback' => TRUE,
    'page callback' => 'gallery_get_page',
    'page arguments' => array('gallery-home', 'foto'),
  );
  $menu['gallery/video'] = array(
    'title' => 'Galleria - video',
    'access callback' => TRUE,
    'page callback' => 'gallery_get_page',
    'page arguments' => array('gallery-home', 'video'),
  );
  $menu['gallery/%'] = array(
    'title' => 'Galleria',
    'access callback' => TRUE,
    'page callback' => 'gallery_get_page',
    'page arguments' => array('dettaglio', 1),
  );
  return $menu;
}
<?php
function gallery_theme(){
  $theme = array();
  $theme_path = path_to_theme();
  $theme['gallery-home'] = array('template' => 'gallery-home', 'path'=>$theme_path.'/templates');
  $theme['gallery-dettaglio'] = array('template' => 'gallery-dettaglio', 'path'=>$theme_path.'/templates');
  return $theme;
}
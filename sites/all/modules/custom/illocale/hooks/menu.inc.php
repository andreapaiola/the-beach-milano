<?php
function illocale_menu(){
  $menu = array();
  $menu['the-beach'] = array(
    'title' => 'Il locale',
    'access callback' => TRUE,
    'page callback' => 'illocale_get_page',
    'page arguments' => array('illocale-home'),
  );
  $menu['the-beach/%'] = array(
    'title' => 'Il locale',
    'access callback' => TRUE,
    'page callback' => 'illocale_get_page',
    'page arguments' => array('dettaglio', 1),
  );
  return $menu;
}
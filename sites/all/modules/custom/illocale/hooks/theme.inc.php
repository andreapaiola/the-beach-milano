<?php
function illocale_theme(){
  $theme_path = path_to_theme();
  $theme = array();
  $theme['illocale-home'] = array('template' => 'il-locale-home', 'path'=>$theme_path.'/templates');
  $theme['illocale-dettaglio'] = array('template' => 'il-locale-dettaglio', 'path'=>$theme_path.'/templates');
  return $theme;
}

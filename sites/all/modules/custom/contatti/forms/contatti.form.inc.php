<?php
function contatti_form(){
  global $base_url;
  $form = array(
    '#method' => 'post',
  );

  $form['nome'] = array(
    '#type' => 'textfield',
    '#title' => 'Nome',
    '#default_value' => (isset($_POST['nome'])) ? $_POST['nome'] : '',
    '#required' => TRUE,
    '#attributes' => array('placeholder'=>'Nome *'),
  );

  $form['cellulare'] = array(
    '#type' => 'textfield',
    '#title' => 'Cellulare',
    '#default_value' => (isset($_POST['cellulare'])) ? $_POST['cellulare'] : '',
    '#required' => FALSE,
    '#attributes' => array('placeholder'=>'Cellulare'),
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => 'Email',
    '#default_value' => (isset($_POST['email'])) ? $_POST['email'] : '',
    '#required' => TRUE,
    '#attributes' => array('placeholder'=>'Email *'),
  );

  $form['messaggio'] = array(
    '#type' => 'textarea',
    '#title' => 'Messaggio',
    '#default_value' => (isset($_POST['messaggio'])) ? $_POST['messaggio'] : '',
    '#required' => TRUE,
    '#attributes' => array('placeholder'=>'Messaggio *'),
  );

  $form['privacy'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Consenso Privacy Policy',
    '#options' => array(1 => 'Acconsento al trattamento dei miei dati e dichiaro di aver preso visione della <a href="/informativa" target="_blank">Privacy Policy</a>'),
//    '#prefix' => '<p class="consenso">CONSENSO EX ART 13 DEL DLGS N° 196/03 <sup class="form-required" title="Questo campo è obbligatorio.">*</sup></p>',
    '#required' => TRUE,
    '#attributes' => array(),
  );

  $form['marketing'] = array(
	'#type' => 'checkboxes',
	'#title' => 'Consenso Marketing',
	'#options' => array(1 => 'Acconsento al trattamento dei miei dati personali per finalità di marketing e per ricevere periodicamente informazioni relative alle vostre iniziative promozionali e commerciali inviate per e-mail, sms o WhatsApp'),
	'#required' => false,
	'#attributes' => array(),
  );

  $form['invia'] = array(
    '#type' => 'submit',
    '#value' => 'Invia',
    '#attributes' => array(),
    '#weight' => 200,
  );
  return $form;
}

function contatti_form_validate(&$form, &$form_state){
  $values = $form_state['values'];

  //$values['nome'];
  //
  if($values['cellulare'] != '' && !preg_match('/^([\+]?)([0-9\s]{5,20})$/', $values['cellulare'])){
    form_set_error('cellulare', "Il numero di telefono inserito non può essere accettato.");
  }
  if(!valid_email_address($values['email'])){
    form_set_error('email', "L'indirizzo Email inserito non risulta valido.");
  }
  $form_state['values']['messaggio'] = strip_tags($form_state['values']['messaggio']);


    //Hijack our errors so they don't display normally.
    $errors = form_get_errors();
    //Only rebuild form if there are actually errors that we need to display
    if( !empty($errors) ) {
        $form_state['errors'] = $errors;
        $form['errors']= array(
            '#type' => 'item',
            '#title' => 'Errori',
            '#markup' => '<p class="errors">'.implode('<br>',$errors).'</p>',
            '#weight' => -1
        );
        if( isset($errors['privacy']) && !empty($errors['privacy']))
        {
            $form['privacy']['#attributes'] = array('class'=>array('error'));
        }
        $form_state['rebuild'] = TRUE;
        //dsm($errors);
        //dsm($form);
        //dsm($form_state);
    }
    // Remove all error messages
    drupal_get_messages('error');
}

function contatti_form_submit($form, &$form_state){
  $values = $form_state['values'];
  $insert_values = array();
  if(trim($values['cellulare']) == ''){
    $insert_values = array('nome'=>$values['nome'], 'email'=>$values['email'], 'messaggio'=>$values['messaggio'], 'privacy'=>$values['privacy'], 'marketing'=>$values['marketing']);
  } else {
    $insert_values = array('nome'=>$values['nome'], 'cellulare'=>$values['cellulare'], 'email'=>$values['email'], 'messaggio'=>$values['messaggio'], 'privacy'=>$values['privacy'], 'marketing'=>$values['marketing']);
  }
  if(isset($_POST['fbdata'])){
    $insert_values['fb_data'] = serialize($_POST['fbdata']);
  }
  $query = db_insert('the_beach_contatti');
  $query->fields($insert_values);
  $id = $query->execute();

  $to = 'thebeachmilano.com@gmail.com'; //info@thebeachmilano.com
  $subject = 'TheBeach - Richiesta di informazioni ['.$id.']';
  $body = contatti_get_body_mail($values);
  $headers = contatti_get_header_mail(true);
  mail($to, $subject, $body, $headers);

  drupal_set_message('La tua richiesta è stata inviata allo staff, ti risponderemo al più presto.');
}

function contatti_get_header_mail($bcc = false){
  //$to = 'Marco Delle Feste<marco.dellefeste@diginventa.it>, Livio Giovenco<livio.giovenco@diginventa.it>'; //Info The beach Milano<info@thebeachmilano.com>
  $from = variable_get('site_mail', 'noreply@local.diginventa.it');
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

// Additional headers
  //$headers .= 'To: '.$to . "\r\n";
  if($bcc){
	  $headers .= "Bcc: livio.giovenco@diginventa.it\r\n";
  }
  $headers .= 'From: The Beach Milano Site <'.$from.'>' . "\r\n";
  return $headers;
}

function contatti_get_body_mail($values){
  $cellulare = ($values['cellulare'] != '') ? $values['cellulare'] : 'non fornito';
  return 'Data e ora della richiesta: '.date('d-m-Y H:i:s').'<br />
Nome: '.$values['nome'].'<br />
Cellulare: '.$cellulare.'<br />
E-mail: '.$values['email'].'<br />
Messaggio: '.$values['messaggio'].'<br />
Consenso Privacy Policy: '.($values['privacy'][1] == 1 ? 'SI' : 'NO').'<br />
Consenso Marketing tramite e-mail, sms o WhatsApp: '.($values['marketing'][1] == 1 ? 'SI' : 'NO');
}




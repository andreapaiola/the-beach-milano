<?php
function contatti_menu(){
  $menu = array();
  $menu['contatti'] = array(
    'title' => 'Contatti',
    'access callback' => TRUE,
    'page callback' => 'contatti_get_page',
  );
  $menu['admin/contatti'] = array(
    'title' => 'Contatti',
    'access callback' => 'contatti_access_callback',
    'page callback' => 'contatti_export',
  );
  return $menu;
}

function contatti_access_callback(){
  global $user;
  if(isset($user->roles)){
    foreach($user->roles as $k=>$nome){
      if($nome == 'Editor'){
        return true;
      }
    }
  }
  return false;
}
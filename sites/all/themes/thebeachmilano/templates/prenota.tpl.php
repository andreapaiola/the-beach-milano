<article>
<div class="top">
    <?php
    $slider=array();
    //@Andrea: questa immagine deve cambiare in base a che form viene restituito, io ti passo il parametro $content['form_type'] che sarà valorizzato
    //come 'ristorante' o 'serata'
    //crea la logica per mostrare l'immagine corretta
    //$slider[]=array('path'=>'editor-upload/ristorante.jpg','alt'=>'cIAO ');
    //$slider[]=array('path'=>'editor-upload/prenota_serata.jpg','alt'=>'Una serata divertente');
    if($content['form_type']=='ristorante') $slider[]=array('path'=>'editor-upload/prenota_un_tavolo.jpg','alt'=>'Il tuo tavolo prenotato');;
    if($content['form_type']=='serata') $slider[]=array('path'=>'editor-upload/prenota_serata.jpg','alt'=>'Una serata divertente');;
    ?>
    <?php require 'slider.php'; ?>
</div>
<div class="bottom">
    <div class="maxwidth">
        <?php //$title='Prenota un tavolo'; ?>
        <h1><?php echo $content['title']; ?></h1>
        <div class="clearfix">

            <div class="col-l">

                <div id="current-choice" class="buttons" style="display: block">
                    <p>
                        Continua con
                        <?php echo l('Facebook connect','facebook-connect',array('attributes'=>array('id'=>'login-app-fb', 'class'=>array('facebook-connect')))); ?>
                    </p>
                    <p>
                        Oppure
                        <?php echo l('Compila manualmente il form','compila-form',array('attributes'=>array('id'=>'no-fb-login', 'class'=>array('compila-form'))) ); ?>
                    </p>

                </div>

                <div id="current-form" class="form" style="display: none">
<?php
print drupal_render($content['form']);
?>
                </div>
            </div>
            <div class="col-r">
                <?php
                //@Andrea: l'immagine laterale sarà dinamica nel caso del form_type == serata
                //sarà statica per form_type == ristorante
                //in caso sia serata ti passo il paramentro $content['form_immagine'] che conterrà un array da assegare a $ristorante.
                //crea la logica per mostrare l'immagine corretta
                $ristorante = array();
                /*for($i=1;$i<3;$i++)
                {
                    $ristorante[]=array(
                      'path'=>'editor-upload/ristorante2.jpg',
                      'alt'=>'altitittino',
                      'title'=>'altitittino'
                    );
                }*/
                if($content['form_type']=='ristorante')
                {
                    $ristorante[]=array('path'=>'editor-upload/prenota_ristorante.jpg');
                }
                if($content['form_type']=='serata')
                {
                    $ristorante=$content['form_immagine'];
                }
                ?>
                <?php foreach($ristorante as $item): ?>
                    <div class="item">
                        <?php echo responsive_image( $item['path'] ); ?>
                    </div>
                <?php endforeach; ?>
            </div>
            </div>
        </div>
    </div>
</article>
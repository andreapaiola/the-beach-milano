<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */

//var_dump($title);
//var_dump($content);
//var_dump($content["field_gallery_foto_correlata"]);


?>
<article class="<?php print $classes; ?> clearfix node-<?php print $node->nid; ?>"<?php print $attributes; ?>>

  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    //print render($content);
    //print render($content['field_immagine_il_locale']);

  //var_dump($content['field_immagine_il_locale'][0]["#item"]);
  ?>
  <div class="top">
    <?php //echo responsive_image($content['field_immagine_pagina'][0]["#item"]["uri"],array('alt'=>$content['field_immagine_pagina'][0]["#item"]["alt"])); ?>
    <?php
    $slider=array();
    $slider[]=array('path'=>$content['field_immagine_pagina'][0]["#item"]["uri"],'alt'=>$content['field_immagine_pagina'][0]["#item"]["alt"], 'title'=>$content['field_immagine_pagina'][0]["#item"]["title"]);
    ?>
    <?php require 'slider.php'; ?>
  </div>
  <div class="bottom">
  <div class="maxwidth">
    <h1><?php echo $title; ?></h1>
    <div class="bodytxt">
      <?php print render($content["body"]); ?>
    </div>

  </div>
  </div>


</article>

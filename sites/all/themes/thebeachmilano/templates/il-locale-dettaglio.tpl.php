<?php
$node = $content['dati']['node'];
$prev = $content['dati']['prev'];
$next = $content['dati']['next'];
?>
<article class="<?php print $classes; ?> clearfix node-<?php print $node->nid; ?>"<?php print $attributes; ?>>

  <div class="top">
    <?php //echo responsive_image($content['field_immagine_il_locale'][0]["#item"]["uri"],array('alt'=>$content['field_immagine_il_locale'][0]["#item"]["alt"])); ?>
    <?php
    $slider=array();
    foreach($node->field_immagine_il_locale[LANGUAGE_NONE] as $k=>$file){
      $slider[]=array('path'=>$file['uri'],'alt'=>$file['alt'], 'title'=>$file['title']);
    }
    ?>
    <?php require 'slider.php'; ?>
  </div>
  <div class="bottom">
  <div class="maxwidth">
    <h1><?php echo $node->title; ?></h1>
    <div class="bodytxt">
      <?php
      if(isset($node->body[LANGUAGE_NONE][0])){
        print render($node->body[LANGUAGE_NONE][0]['safe_value']);
      }
      ?>
    </div>
    <?php require 'share.php'; ?>
    <?php
    //print render($content["field_gallery_foto_correlata"]["field_immagine_di_anteprima"]);
    //var_dump($content["field_gallery_foto_correlata"]);

    ?>
    <?php
    $events=$content['dati']['gallery'];
    ?>
    <div class="related clearfix">
      <?php foreach ($events as $key=>$item): ?>
        <div class="item">
          <?php echo l(responsive_image( $item['path'], array('alt'=>$item['alt'],'title'=>$item['title']) ),$item['href'],array('html'=>true,'fragment'=>'item-'.$key)); ?>
        </div>
      <?php endforeach; ?>
    </div>

    <nav class="prev-next clearfix">
      <?php if(!empty($prev)){
        echo l($prev['label'],$prev['link'],array('attributes'=>array('class'=>array('prev'))));
      }
      ?>
      <?php if(!empty($next)){
        echo l($next['label'],$next['link'],array('attributes'=>array('class'=>array('next'))));
      }
      ?>
    </nav>

  </div>
  </div>


</article>

<div class="custom-box box-bottom box-bottom-<?php echo count($rooms); ?>">
    <div class="custom-box-content clearfix">
        <?php foreach( $rooms as $item ): ?>
            <div class="item">
                <?php echo l(responsive_image( $item['path'], array('alt'=>$item['alt'],'title'=>$item['imgtitle']) ).'<b>'.truncate_utf8(trim(strip_tags($item['title'])),28,true,true).'</b>',$item['href'],array('html'=>true)); ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
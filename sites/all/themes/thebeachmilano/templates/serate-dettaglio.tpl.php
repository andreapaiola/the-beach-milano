<?php
//print print_r($content);
$prev = $content['serate']['prev'];
$next = $content['serate']['next'];
?>
<div class="maxwidth">
    <div class="night clearfix">
        <div class="col-l<?php if(!((isset($content['serate']['correlati']) && !empty($content['serate']['correlati'])) || (isset($content['serate']['gallery']) && !empty($content['serate']['gallery']))) ): ?> col-l-large<?php endif; ?>">
            <?php
            $night = $content['serate']['nodo'];
            $slider = $night['slider'];
            ?>
            <?php require 'slider.php'; ?>
            <div class="col-l-bottom">
            <h2><?php echo format_date($night['date'],'custom','l j F Y'); ?></h2>
            <h1><?php echo $night['title']; ?></h1>
            <div class="bodytxt">
                <?php echo render($night['body']); ?>
            </div>
            <?php echo l('JOIN GUESTLIST','serate/iscriviti-alla-lista',array('query'=>array('serata'=>$night['nid']), 'attributes'=>array('class'=>array('iscriviti')))); ?>
            <?php echo l('Info & Reservation WhatsApp','https://wa.me/+393517305441',array('absolute' => TRUE,'attributes'=>array('class'=>array('whatsappbutton')))); ?>
            <?php require 'share.php'; ?>
            <nav class="prev-next clearfix"><?php if($prev != '') { echo l('Precedente',$prev,array('attributes'=>array('class'=>array('prev')))); } ?> <?php if($next != ''){ echo l('Successivo',$next,array('attributes'=>array('class'=>array('next')))); } ?></nav>
        </div>
        </div>
        <?php if( (isset($content['serate']['correlati']) && !empty($content['serate']['correlati'])) || (isset($content['serate']['gallery']) && !empty($content['serate']['gallery'])) ): ?>
        <div class="col-r">
            <?php if(isset($content['serate']['correlati']) && !empty($content['serate']['correlati'])): ?>
            <h2>Eventi correlati</h2>
            <?php
            $events=$content['serate']['correlati'];
            ?>
            <div class="related">
                <?php foreach ($events as $item): ?>
                    <div class="item clearfix">
                        <?php echo l(responsive_image( $item['path'], array('alt'=>$item['alt'],'title'=>$item['imgtitle']) ).'<span>'.format_date($item['date'],'custom','l j F Y').'<b>'.$item['title'].'</b></span>',$item['href'],array('html'=>true)); ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
            <?php if(isset($content['serate']['gallery']) && !empty($content['serate']['gallery'])): ?>
            <div class="gallery">
                <h2>Gallery dell’ evento</h2>
                <?php
                $rooms=$content['serate']['gallery'];
                ?>
                <div class="custom-box box-bottom box-related-gallery">
                    <div class="custom-box-content clearfix">
                        <?php foreach( $rooms as $item ): ?>
                            <div class="item">
                                <?php echo l(responsive_image( $item['path'], array('alt'=>$item['alt'],'title'=>$item['imgtitle']) ).'<span>'.format_date($item['date'],'custom','l j F Y').'</span><b>'.truncate_utf8(trim(strip_tags($item['title'])),28,true,true).'</b>',$item['href'],array('html'=>true)); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    </div>
</div>

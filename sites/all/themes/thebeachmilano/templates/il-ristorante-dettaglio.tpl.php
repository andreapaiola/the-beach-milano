<?php
$node = $content['dati']['node'];
?>
<article class="<?php print $classes; ?> clearfix node-<?php print $node->nid; ?>"<?php print $attributes; ?>>

  <div class="top">
    <?php //echo responsive_image('editor-upload/ristorante.jpg',array('alt'=>'altino')); ?>
    <?php
    $slider=array();
    foreach($node->field_immagine_il_ristorante[LANGUAGE_NONE] as $k=>$file){
      $slider[]=array('path'=>$file['uri'],'alt'=>$file['alt'],'title'=>$file['title']);
    }
    ?>
    <?php require 'slider.php'; ?>
  </div>
  <div class="bottom">
    <div class="maxwidth">
      <h1><?php echo $node->title; ?></h1>

      <div class="clearfix">

        <div class="col-l">
        <div class="bodytxt">
        <?php //print render($content["body"]); ?>
        <?php
        $menu = $content['dati']['menu'];
        ?>

          <?php foreach ($menu as $menuitem): ?>
              <div class="menuitem">
                <h2><?php echo $menuitem['tipo']; ?></h2>
                <table>
                  <tbody>
                  <?php foreach ($menuitem['portate'] as $portateitem): ?>
                      <tr>
                      <td class="nome"><?php echo $portateitem['nome']; ?></td>
                      <td class="prezzo"><?php echo $portateitem['prezzo']; ?></td>
                      </tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
          <?php endforeach; ?>
          <div class="bodytxt">
            <?php
            if(isset($node->body[LANGUAGE_NONE][0])){
              print render($node->body[LANGUAGE_NONE][0]['safe_value']);
            }
            ?>
          </div>

        </div>
          <?php
            $tipo = '';
            switch(strtolower($node->title)){
              case 'aperitivo':
                $tipo = 'aperitivo';
                break;
              case 'cena':
                $tipo = 'cena_menu_fisso';
                break;
              case 'alla carta':
                $tipo = 'cena_alla_carta';
                break;
            }
          ?>
          <!--
          <?php echo l('Prenota un tavolo','ristorante/prenota-un-tavolo',array('query'=>array('tipo'=>$tipo), 'attributes'=>array('class'=>array('iscriviti')))); ?>
          -->
          <?php echo l('Info & Reservation WhatsApp','https://wa.me/+393517305441',array('absolute' => TRUE,'attributes'=>array('class'=>array('whatsappbutton')))); ?>
        </div>
         <div class="col-r">
           <?php
            $ristorante = $content['dati']['gallery'];
          ?>
           <?php foreach($ristorante as $item): ?>
               <div class="item">
               <?php echo responsive_image( $item['path'], array('alt'=>$item['alt'],'title'=>$item['title']) ); ?>
               </div>
           <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</article>

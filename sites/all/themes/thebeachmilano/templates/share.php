<div class="share">
    <p>Condividi:</p>
    <?php
    $path='serate/giorno/stica';
    $services = array(
        'Facebook'=>'https://www.facebook.com/sharer/sharer.php?u='
        ,'Twitter'=>'ttp://twitter.com/share?url='
        ,'GPlus'=>'https://plus.google.com/share?url='
    );
    ?>
    <?php foreach( $services as $key=>$endpoint ): ?>
        <?php echo l('Share on '.$key,$endpoint.urlencode(url($path,array('absolute'=>true))),array('absolute'=>true,'attributes'=>array('target'=>'_blank','class'=>array($key)))); ?>
    <?php endforeach; ?>
</div>
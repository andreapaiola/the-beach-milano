<?php
//print print_r($content);
?>
<div class="gallery-top">
    <div class="galleria">
        <?php
        $nodo = $content['dati']['node'];
        $gallery=$content['dati']['gallery'];
        ?>
        <?php foreach( $gallery as $item ): ?>
            <?php //echo responsive_image( $item['path'], array('alt'=>$item['alt']) ); ?>
            <?php
            $variables = array(
                'path' => $item['path'],
                'alt' => $item['alt'],
                'title' => $item['title']
            );
            echo theme('image', $variables);
            ?>
        <?php endforeach; ?>
    </div>

    <div class="maxwidth gallery-dettaglio-txt">
    <?php
    $night = array(
      'title'=>$nodo->title,
      'date'=> strtotime($nodo->field_data[LANGUAGE_NONE][0]['value']),
    );
    ?>
    <h2><?php echo format_date($night['date'],'custom','l j F Y'); ?></h2>
    <h1><?php echo $night['title']; ?></h1>
    <br>
        <?php echo l('Torna indietro','javascript:history.back()',array('external'=>true,'attributes'=>array('class'=>array('iscriviti')))); ?>
    <?php require 'share.php'; ?>

    </div>
</div>
<?php
$rooms = $content['dati']['correlati'];
if(!empty($rooms)){
?>

<div class="gallery-bottom maxwidth">
    <h2>Guarda altri album simili</h2>


    <div class="custom-box box-bottom box-bottom-gallery">
        <div class="custom-box-content clearfix">
            <?php foreach( $rooms as $item ): ?>
                <div class="item">
                    <?php echo l(responsive_image( $item['path'], array('alt'=>$item['alt'],'title'=>$item['imgtitle']) ).'<span>'.format_date($item['date'],'custom','l j F Y').'</span><b>'.truncate_utf8(trim(strip_tags($item['title'])),28,true,true).'</b>',$item['href'],array('html'=>true)); ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php } ?>

<div class="custom-box box-top">
    <div class="custom-box-content clearfix">
        <?php require 'slider.php'; ?>
        <div class="txt">
            <?php if(isset($boxCustomTopText)): ?>
                <?php echo $boxCustomTopText; ?>
            <?php else: ?>
            <h1>Lorem ipsum dolor sit amet</h1>

            <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </p>
            <p>
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
            </p>
            <?php endif; ?>
        </div>
    </div>
</div>
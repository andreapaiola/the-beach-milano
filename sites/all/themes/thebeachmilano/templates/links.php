<nav class="links">
    <?php echo l('The Beach Milano @ Instagram','https://www.instagram.com/thebeachclubmilano/',array('attributes'=>array('target'=>'_blank','class'=>array('instagram')),'absolute'=>true)); ?>
    <?php echo l('The Beach Milano @ Facebook','https://www.facebook.com/thebeachmilano1',array('attributes'=>array('target'=>'_blank','class'=>array('facebook')),'absolute'=>true)); ?>
    <?php //echo l('Capodanno','node/26',array('attributes'=>array('class'=>array('prevendite')))); ?>
    <?php //echo l('Rimani aggiornato','rimani-aggiornato',array('attributes'=>array('class'=>array('rimani')))); ?>
    <?php
    $usa_bottone = variable_get('usa_bottone_personalizzato', 0);
    if($usa_bottone == 1){
        $label = variable_get('prevendite_label', null);
        $nid = variable_get('prevendite_link', null);
        echo l($label,'node/'.$nid,array('attributes'=>array('class'=>array('prevendite'))));
    } ?>
</nav>
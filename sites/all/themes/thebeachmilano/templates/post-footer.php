<div class="post-footer">
    &copy; <?php echo date("Y"); ?> The Beach Milano - P.IVA 05462630962 - <?php echo l('Privacy policy','privacy-policy'); ?> - <?php echo l('Note legali','note-legali'); ?> - <?php echo l('Cookie policy','cookie-policy'); ?> - <?php echo l('Sitemap','sitemap.xml',array('attributes'=>array('target'=>'_blank'))); ?>
</div>
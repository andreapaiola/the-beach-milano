<?php
/**
 * @file
 * Returns the HTML for frontpage.
 */
?>
<main role="main">
    <div class="cover">
        <div class="hi">
            <h1><img src="<?php echo base_path().path_to_theme(); ?>/img/the-beach.png" alt="THE BEACH MILANO" /></h1>
            <?php echo l('ENTRA','homepage'); ?>
            <?php echo l('WHATSAPP','https://wa.me/+393517305441',array('absolute' => TRUE,'attributes'=>array('target'=>'_blank','class'=>array('notuppercase')))); ?>

        </div>
    </div>
    <div class="tv">
        <div class="screen mute" id="tv"></div>
    </div>
</main>
<?php require 'post-footer.php'; ?>
<?php print render($page['bottom']); ?>

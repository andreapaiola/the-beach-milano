<?php if ($main_menu): ?>
    <nav class="main-menu" role="navigation">
        <?php
        // This code snippet is hard to modify. We recommend turning off the
        // "Main menu" on your sub-theme's settings form, deleting this PHP
        // code block, and, instead, using the "Menu block" module.
        // @see https://drupal.org/project/menu_block
        /*
        print theme('links__system_main_menu', array(
            'links' => menu_navigation_links('main-menu',0),
            'attributes' => array(
                'class' => array('navbar', 'clearfix'),
            ),
            'heading' => array(
                'text' => t('Main menu'),
                'level' => 'h2',
                'class' => array('visually-hidden'),
            ),
        ));*/

        print $primary_navigation;

        ?>
    </nav>
<?php endif; ?>


<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>
  <header class="header" role="banner">
    <div class="maxwidth">
    <?php if ($logo): ?>
      <?php echo l('<img src="'.$logo.'" alt="'.t('Home').'"  class="header__logo-image" />','homepage',array('attributes'=>array('rel'=>'home','class'=>array('header__logo')),'html'=>true)); ?>
    <?php endif; ?>
    <?php print render($page['header']); ?>
    <?php require 'menu.php'; ?>
    <?php require 'links.php'; ?>
    <?php print render($page['navigation']); ?>
    </div>
  </header>
    <main role="main">
      <?php print render($page['highlighted']); ?>
      <a href="#skip-link" class="visually-hidden visually-hidden--focusable" id="main-content">Back to top</a>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </main>
  <?php //print render($page['footer']); ?>
  <footer class="footer">
    <div class="maxwidth">
      <div class="col col-links">
      <?php if ($logo): ?>
        <?php echo l('<img src="'.$logo.'" alt="'.t('Home').'"  class="header__logo-image" />','homepage',array('attributes'=>array('rel'=>'home','class'=>array('header__logo')),'html'=>true)); ?>
        <?php require 'links.php'; ?>
      <?php endif; ?>
        </div>
        <div class="col col-menu">
          <?php require 'menu.php'; ?>
        </div>
      <div class="col col-partner">

      </div>
    </div>
  </footer>
<?php require 'post-footer.php'; ?>
<?php print render($page['bottom']); ?>

<?php
//print print_r($content);
?>

<div class="maxwidth">

    <?php
    $slider = $content['dati'];

    $boxCustomTopText = '<p>The Beach Club è uno dei punti di riferimento della movida notturna milanese, ambientato all’interno di uno spettacolare giardino, propone i party più esclusivi e gli eventi più trendy. 
</p><p>Una location sfavillante e di tendenza fa da cornice al divertimento più assoluto in un clima di mondanità e festa, tra look alla moda, glamour, luccicanti ed eccentrici, accompagnati dalla musica dei migliori deejay, vocalist, animazione e special guests di fama mondiale.

</p><p>Dotato di 2 sale distinte , 4 punti bar , 2 piste da ballo e 3 zone ristorante, il The Beach Club vive più momenti con stile, partendo dal tardo pomeriggio con l\'aperitivo, proseguendo con l\'elegante cena e arrivando fino a notte fonda.


</p><p>The Beach Club non è solo divertimento: è emozione vera!</p>
';

    ?>
    <?php require 'box-custom-top.php'; ?>

    <?php
    $rooms = array();
    $rooms[] = array(
        'title'=>'Main Room'
        ,'alt'=>'Foto Main Room'
        ,'imgtitle'=>'Foto Main Room'
        ,'path'=>'editor-upload/locale-sala1.jpg'
        ,'href'=>'the-beach/main-room'
    );
    $rooms[] = array(
        'title'=>'Blackroom'
        ,'alt'=>'Foto Blackroom'
        ,'imgtitle'=>'Foto Blackroom'
        ,'path'=>'editor-upload/sala2.jpg'
        ,'href'=>'the-beach/blackroom'
    );
    $rooms[] = array(
        'title'=>'Ristorante'
        ,'alt'=>'Foto del ristorante'
        ,'imgtitle'=>'Foto del ristorante'
        ,'path'=>'editor-upload/locale-ristorante.jpg'
        ,'href'=>'the-beach/ristorante'
    );
    $rooms[] = array(
        'title'=>'Estivo'
        ,'alt'=>'Foto Estivo'
        ,'imgtitle'=>'Foto Estivo'
        ,'path'=>'editor-upload/giardino.jpg'
        ,'href'=>'the-beach/estivo'
    );
    ?>
    <?php require 'box-custom-bottom.php'; ?>

</div>
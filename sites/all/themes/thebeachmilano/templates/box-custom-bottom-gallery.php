<div class="new-gallery">
    <?php
    $filtra=!is_null($categorie);
    ?>
    <?php if($filtra): ?>
        <div class="filtra">
        <h2>Filtra per categoria</h2>
        <form id="filtra-per-categoria">
            <select id="cambia-categoria">
               <?php foreach($categorie as $item): ?>
                   <option value="<?php echo $item['value']; ?>" <?php print $item['selected']; ?>><?php echo $item['label']; ?></option>
                <?php endforeach; ?>
            </select>
        </form>
        </div>

    <?php endif; ?>
    <div class="new-gallery-content clearfix">
        <?php foreach( $rooms as $item ): ?>
            <div class="item">
                <?php echo l(responsive_image( $item['path'], array('alt'=>$item['alt'],'title'=>$item['imgtitle']) ).''.format_date($item['date'],'custom','l j F Y').' <b>'.truncate_utf8(trim(strip_tags($item['title'])),28,true,true).'</b>',$item['href'],array('html'=>true)); ?>
            </div>
        <?php endforeach; ?>
    </div>
    <?php $display = ( isset($content['dati']['link_load_more']) && !empty($content['dati']['link_load_more'])) ? 'style="display:block"' : 'style="display:none"'; ?>
    <div class="load" <?php print $display; ?>>
        <?php echo l('Load more',$content['dati']['link_load_more'],array('attributes'=>array('class'=>array('load-more')))); ?>
    </div>
</div>
<?php
$nights = $content['serate'];

?>

<div class="nights">
    <?php foreach( $nights as $item ): ?>
        <div class="item">
            <?php echo l(responsive_image( $item['path'], array('alt'=>$item['alt'],'title'=>$item['imgtitle']) ).format_date($item['date'],'custom','l j F Y').'<b>'.truncate_utf8(trim(strip_tags($item['title'])),28,true,true).'</b>',$item['href'],array('html'=>true)); ?>
        </div>
    <?php endforeach; ?>
</div>

<?php
//print print_r($content);
?>
<div class="maxwidth">

    <?php
    $slider = $content['dati'];



    $boxCustomTopText = '
<p>Il ristorante del The Beach Club ha una sola parola d’ordine: qualità. Grazie alla grande cura nella selezione dei prodotti e alla preparazione dei piatti, il The Beach Club propone il meglio della cucina mediterranea offrendo menu stagionali realizzati da chef di primo livello e un servizio puntuale.
<br>Con una capienza di oltre cento coperti il The Beach Club è location ideale per una cena intima ma anche per eventi di gruppo o aziendali, cerimonie private e aperitivi.</p>';



    ?>
<?php require 'box-custom-top.php'; ?>
    <?php
    $rooms = array();
    /*
    $rooms[] = array(
        'title'=>'Ristorante'
        ,'alt'=>'Foto del ristorante'
        ,'imgtitle'=>'Foto del ristorante'
        ,'path'=>'editor-upload/ristorante1.jpg'
        ,'href'=>'ristorante/ristorante'
    );
    */
    $rooms[] = array(
        'title'=>'Aperitivo'
        ,'alt'=>'Foto aperitivo'
        ,'imgtitle'=>'Foto aperitivo'
        ,'path'=>'editor-upload/ristorante-aperitivo.jpg'
        ,'href'=>'ristorante/aperitivo'
    );
    $rooms[] = array(
        'title'=>'Cena menu fisso'
        ,'alt'=>'Foto cena'
        ,'imgtitle'=>'Foto cena'
        ,'path'=>'editor-upload/ristorante-cena.jpg'
        ,'href'=>'ristorante/cena'
    );
    $rooms[] = array(
        'title'=>'Cena alla carta'
        ,'alt'=>'Foto alla carta'
        ,'imgtitle'=>'Foto alla carta'
        ,'path'=>'editor-upload/ristorante-alla-carta.jpg'
        ,'href'=>'ristorante/alla-carta'
    );
    $rooms[] = array(
        'title'=>'Prenota un tavolo'
        ,'alt'=>'Foto tavolo prenotato'
        ,'imgtitle'=>'Foto tavolo prenotato'
        ,'path'=>'editor-upload/ristorante-prenota.jpg'
        ,'href'=>'ristorante/prenota-un-tavolo'
    );
    
    ?>
    <?php require 'box-custom-bottom.php'; ?>

</div>


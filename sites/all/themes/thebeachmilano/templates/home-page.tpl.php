<?php
$slider = $content['slider'];

?>
<?php require 'slider.php'; ?>
<h1>Prossime serate</h1>
<div class="maxwidth">
    <?php require 'nights.php'; ?>
</div>

<?php
$usa_overlay = variable_get('mostra_overlay', 0);
if($usa_overlay){
    $url = variable_get('url_overlay', '#');
    $id_image = variable_get('immagine_overlay', null);
    $image = file_load($id_image);
    $url_image = base_path().drupal_get_path('theme','thebeachmilano').'/img/overlay.png';
    $width = 300;
    $height = 300;
    if(!is_null($image)){
        $url_image = file_create_url($image->uri);
        $image_info = image_get_info($image->uri);
        $width = $image_info['width'];
        $height = $image_info['height'];
    }

?>
<div class="overlay custom-overlay">
    <div class="overlay-content">
        <a href="#close" class="close">X</a>
        <a href="<?php print $url; ?>"><img src="<?php echo $url_image; ?>" width="<?php print $width; ?>" height="<?php print $height; ?>" /></a>
    </div>
</div>
<script>
    jQuery(document).ready(function($){
        $('.overlay a[href="#close"]').click(function(event){
            event.preventDefault();
            $('.overlay').remove();
        });
        $('.overlay').click(function(event){
            if( this==event.target )
            {
                event.preventDefault();
                $('.overlay').remove();
            }
        });
        $('.custom-overlay .overlay-content').css('max-width',$('.custom-overlay .overlay-content img').attr('width')+'px');
    });
</script>
<?php
} //endif $usa_overlay
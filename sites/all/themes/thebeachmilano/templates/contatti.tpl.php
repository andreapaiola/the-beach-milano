<?php
//print_r($content);
//print_r($form);
?>
<div class="txt-contatti">
    <h1>Contatti</h1>
    <p>THE BEACH – Via A. Corelli 62 – Ingresso da Via Taverna – Milano
        <br>Tel.: +39 02 9455 7574 - +39 351 730 5441
    </p>

    <?php echo l('WhatsApp','https://wa.me/+393517305441',array('absolute' => TRUE,'attributes'=>array('target'=>'_blank','class'=>array('whatsappbutton','whatsappbutton-centered','notuppercase')))); ?>


</div>
<div id="current-choice" class="buttons" style="display: block">
  <p>
    Continua con
    <?php echo l('Facebook connect','facebook-connect',array('attributes'=>array('id'=>'login-app-fb', 'class'=>array('facebook-connect')))); ?>
  </p>
  <p>
    Oppure
    <?php echo l('Compila manualmente il form','compila-form',array('attributes'=>array('id'=>'no-fb-login', 'class'=>array('compila-form'))) ); ?>
  </p>

</div>
<div id="current-form" class="form-contatti" style="display: none;">
<?php echo drupal_render($form); ?>
</div>
<div id="map"></div>

<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function thebeachmilano_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  thebeachmilano_preprocess_html($variables, $hook);
  thebeachmilano_preprocess_page($variables, $hook);
}
// */


function thebeachmilano_css_alter(&$css) {

    if (!module_exists('libraries')) return;

    $items = array(
        'jquery.ui.core',
        'jquery.ui.datepicker',
        'jquery.ui.theme',
        'jquery.ui.button',
        'jquery.ui.accordion',
        'jquery.ui.tooltip',
        'jquery.ui.resizable',
        'jquery.ui.dialog',
        'jquery.ui.autocomplete',
    );
    foreach ($items as $key => $value) {
        if (isset($css['misc/ui/' . $value . '.css'])) {
            $css['misc/ui/' . $value . '.css']['data'] = libraries_get_path('jquery') . '/ui/minified/' . $value . '.min.css';
        }
    }

}

function thebeachmilano_js_alter(&$js) {

    if (!module_exists('libraries')) return;

    $items = array(
        'jquery-1.4.4-uncompressed.js'     => array(
            'data'     => 'jquery.js',
            'versione' => '3.1.1'
        ),
        'jquery.js'                        => array(
            'data'     => 'jquery.min.js',
            'versione' => '3.1.1'
        ),
        'jquery.once.js'                   => array(
            'data'     => 'jquery.once.min.js',
            'versione' => '1.2.6'
        ),
        'jquery.cookie.js'                 => array(
            'data'     => 'jquery.cookie.js',
            'versione' => '1.4.0'
        ),

        'ui/jquery.ui.core.min.js'         => array(
            'data'     => 'ui/minified/jquery.ui.core.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.datepicker.min.js'   => array(
            'data'     => 'ui/minified/jquery.ui.datepicker.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.widget.min.js'       => array(
            'data'     => 'ui/minified/jquery.ui.widget.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.button.min.js'       => array(
            'data'     => 'ui/minified/jquery.ui.button.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.position.min.js'     => array(
            'data'     => 'ui/minified/jquery.ui.position.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.draggable.min.js'    => array(
            'data'     => 'ui/minified/jquery.ui.draggable.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.resizable.min.js'    => array(
            'data'     => 'ui/minified/jquery.ui.resizable.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.mouse.min.js'        => array(
            'data'     => 'ui/minified/jquery.ui.mouse.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.dialog.min.js'       => array(
            'data'     => 'ui/minified/jquery.ui.dialog.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.effect.min.js'       => array(
            'data'     => 'ui/minified/jquery.ui.effect.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.accordion.min.js'    => array(
            'data'     => 'ui/minified/jquery.ui.accordion.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.tooltip.min.js'      => array(
            'data'     => 'ui/minified/jquery.ui.tooltip.min.js',
            'versione' => '1.10.4'
        ),
        'ui/jquery.ui.autocomplete.min.js' => array(
            'data'     => 'ui/minified/jquery.ui.autocomplete.min.js',
            'versione' => '1.10.4'
        ),
    );
    foreach ($items as $key => $value) {
        if (isset($js['misc/' . $key])) {
            $js['misc/' . $key]['data']    = libraries_get_path('jquery') . '/' . $value['data'];
            $js['misc/' . $key]['version'] = $value['versione'];
        }

    }
    if (isset($js[drupal_get_path('module', 'devel') . '/jquery-1.4.4-uncompressed.js'])) {
        $js[drupal_get_path('module', 'devel') . '/jquery-1.4.4-uncompressed.js']['data']    = libraries_get_path('jquery') . '/jquery.js';
        $js[drupal_get_path('module', 'devel') . '/jquery-1.4.4-uncompressed.js']['version'] = '3.1.1';
    }
    if (isset($js[drupal_get_path('module', 'views') . '/js/ajax_view.js'])) {
        $js[drupal_get_path('module', 'views') . '/js/ajax_view.js']['data']    = libraries_get_path('views') . '/ajax_view.js';
        $js[drupal_get_path('module', 'views') . '/js/ajax_view.js']['version'] = '3.1.1';
    }

}

/**
 * Override or insert variables into the html templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("html" in this case.)
 */

function thebeachmilano_preprocess_html(&$variables, $hook) {
  if (!module_exists('libraries')) return;

  drupal_add_js(libraries_get_path('console') . '/console.js', 'file');
  drupal_add_js(libraries_get_path('picturefill') . '/picturefill.min.js', 'file');
  drupal_add_js(libraries_get_path('jquery') . '/jquery.touchSwipe.min.js', 'file');
  drupal_add_js(libraries_get_path('jquery') . '/smartresize.js', 'file');

  if(drupal_is_front_page()){
      $element = array(
          '#tag' => 'meta',
          '#attributes' => array(
              "property" => 'og:title',
              "content" => 'The Beach Milano - home',
          ),
      );
      drupal_add_html_head($element,'og_title');
      $element = array(
          '#tag' => 'meta',
          '#attributes' => array(
              "property" => 'og:description',
              "content" => 'The Beach Club è uno dei punti di riferimento della movida notturna milanese, ambientato all’interno di uno spettacolare giardino, propone i party più esclusivi e gli eventi più trendy.',
          ),
      );
      drupal_add_html_head($element,'og_description');
      drupal_add_js(path_to_theme().'/js/front.js', 'file');
  }
  else {
    drupal_add_js(path_to_theme().'/js/not-front.js', 'file');
  }

  $meta_ie_render_engine = array(
    '#type'       => 'html_tag',
    '#tag'        => 'meta',
    '#attributes' => array(
      'content'    => 'IE=edge,chrome=1',
      'http-equiv' => 'X-UA-Compatible',
    ),
    '#weight'     => '-999',
  );
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');

  $keywords = array(
    '#tag' => 'meta',
    '#attributes' => array(
      "name" => 'keywords',
      "content" => 'The Beach, Milano, Club, Serate, Eventi, Lista, Black room, Ristorante, Cena',
    ),
  );
  drupal_add_html_head($keywords,'keywords');
  global $base_url;
  $og_image = array(
    '#tag' => 'meta',
    '#attributes' => array(
      "property" => 'og:image',
      "content" => $base_url.'/sites/all/themes/thebeachmilano/img/the-beach-share.jpg',
    ),
  );
  drupal_add_html_head($og_image,'og_image');

  $font = array(
    '#type' => 'markup',
    '#markup' => '<link href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:300,400,700" rel="stylesheet" type="text/css">'."\n"
  );
  drupal_add_html_head($font, 'sansserif');
  $font = array(
    '#type' => 'markup',
    '#markup' => '<link href="//fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css">'."\n"
  );
  drupal_add_html_head($font, 'sansserif2');
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("page" in this case.)
 */

function thebeachmilano_preprocess_page(&$variables, $hook) {
    $menu_tree = menu_tree_all_data('main-menu');
    $tree_output_prepare = menu_tree_output($menu_tree);
    $variables['primary_navigation'] = drupal_render($tree_output_prepare);
}


/**
 * Override or insert variables into the region templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function thebeachmilano_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--no-wrapper.tpl.php template for sidebars.
  if (strpos($variables['region'], 'sidebar_') === 0) {
    $variables['theme_hook_suggestions'] = array_diff(
      $variables['theme_hook_suggestions'], array('region__no_wrapper')
    );
  }
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function thebeachmilano_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  if ($variables['block_html_id'] == 'block-system-main') {
    $variables['theme_hook_suggestions'] = array_diff(
      $variables['theme_hook_suggestions'], array('block__no_wrapper')
    );
  }
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function thebeachmilano_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // thebeachmilano_preprocess_node_page() or thebeachmilano_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function thebeachmilano_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

function thebeachmilano_date_nav_title($params) {
  return t(date('F')).' '.date('Y');
}

/*
function thebeachmilano_form_alter(&$form, &$form_state, $form_id) {

    if( $form_id=='prenota_tavolo_form' )
    {

        //drupal_set_message('<pre>' . print_r($form, TRUE) . '</pre>');
        //drupal_set_message('<pre>' . print_r($form_state, TRUE) . '</pre>');
        //drupal_set_message('<pre>' . print_r($form_id, TRUE) . '</pre>');


        dsm($form_state);
        dsm($form);
    }
}
*/

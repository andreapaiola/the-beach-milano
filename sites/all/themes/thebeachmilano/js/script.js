/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */


(function ($) {

    var time = 6000;
    var start = 1;

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    $.fn.slider = function () {
        return this.each(function (index, item) {

            var uuid = guid();

            var slider = $(item);
            var items = slider.children('.item');
            var itemsNumber = items.length;

            if (itemsNumber > 1) {


                var baseZIndex = parseInt($(items).first().css('z-index'), 10);
                //console.log(baseZIndex);

                //console.log(items);

                var nav = $('<nav>');
                $(items).each(function (index, item) {
                    var id = 'slider-' + uuid + '-' + (index + 1);
                    $(item).attr('id', id).css('z-index', baseZIndex + itemsNumber - index);
                    nav.append('<a href="#' + id + '">' + (index + 1) + '</a>');
                });
                slider.append(nav);

                var viewSlide = function (slide) {
                    var slideItem = $(slide);
                    $(items).not(slideItem).hide();
                    slideItem.fadeIn();
                    var links = slider.find('nav a').removeClass('active');
                    slider.find('nav a[href="' + slide + '"]').addClass('active');
                };

                slider.find('nav a').click(function (event) {
                    event.preventDefault();
                    viewSlide($(this).attr('href'));
                });

                var actualSlide = start;

                var goToSlide = function (actualSlide) {
                    slider.find('nav a').eq(actualSlide - 1).trigger('click');
                }

                var nextSlide = function () {
                    if (actualSlide > itemsNumber - 1) {
                        actualSlide = 1;
                    }
                    else {
                        actualSlide++;
                    }
                    //console.log(actualSlide);
                    goToSlide(actualSlide);
                };
                var prevSlide = function () {
                    if (actualSlide < 2) {
                        actualSlide = itemsNumber;
                    }
                    else {
                        actualSlide = actualSlide - 1;
                    }
                    //console.log(actualSlide);
                    goToSlide(actualSlide);
                };

                var slideInterval = setInterval(nextSlide, time);

                viewSlide($(this).attr('href'));
                goToSlide(actualSlide);
                slider.swipe({
                    //Single swipe handler for left swipes
                    swipeLeft: function (event, direction, distance, duration, fingerCount) {
                        //console.log(direction);
                        nextSlide();
                    }
                    , swipeRight: function (event, direction, distance, duration, fingerCount) {
                        //console.log(direction);
                        prevSlide();
                    }
                });
            }
        });
    };
}(jQuery));

(function ($) {
    $.fn.menuMobile = function () {
        return this.each(function (index, item) {
            var menu = $(item);
            if (window.matchMedia("(min-width: 624px)").matches) {
                $('.header .main-menu .is-expanded .open-close').remove();
                $('.header .main-menu .is-expanded').removeClass('is-open is-close');
                $('.header .main-menu').off('click', '.open-close');
            }
            else {
                $('.header .main-menu .is-expanded').each(function (index, item) {
                    $(item).append('<a href="#open-close" class="open-close">Open/Close</a>');
                    $('.header .main-menu .is-expanded').addClass('is-close');
                });
                $('.header .main-menu').on('click', '.open-close', function (event) {
                    event.preventDefault();
                    var parent = $(this).parent();
                    if (parent.hasClass('is-close')) {
                        parent.removeClass('is-close').addClass('is-open');
                    }
                    else {
                        parent.removeClass('is-open').addClass('is-close');
                    }
                });
            }
        });
    };
}(jQuery));


// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
/*
 (function ($, Drupal, window, document) {

 'use strict';

 // To understand behaviors, see https://drupal.org/node/756722#behaviors
 Drupal.behaviors.my_custom_behavior = {
 attach: function (context, settings) {

 // Place your code here.

 }
 };

 })(jQuery, Drupal, this, this.document);
 */

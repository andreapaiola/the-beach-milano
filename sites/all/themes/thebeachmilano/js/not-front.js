jQuery(document).ready(function($){
    var mainMenu = $('.header .main-menu');
    mainMenu.menuMobile();
    $(window).smartresize(function() {
        mainMenu.menuMobile();
    });

    /* click on galleria */
    mainMenu.children('.menu').children().eq(3).children('a').first().click(function(event){
        event.preventDefault();
    }).css('cursor','default');

    if(!$('body').hasClass('page-homepage'))
    {
        if (window.matchMedia("screen and (max-width: 623px)").matches) {
            $('html, body').animate({
                scrollTop: $('a[href="#skip-link"]').offset().top
            }, 500);
        }
    }
});
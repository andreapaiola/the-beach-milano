/* FACEBOOK SDK INIT */
window.fbAsyncInit = function() {
    FB.init({
        appId      : '1195194837215971',
        xfbml      : true,
        version    : 'v2.8',
        status     : true
    });
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/it_IT/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

jQuery(document).ready(function($){
    var fb_get_me = function(){
        if(typeof FB != 'undefined'){
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB.api('/me', {fields: 'email,id,cover,name,first_name,middle_name,last_name,age_range,link,gender,locale,picture,timezone,updated_time,verified'}, function(meResponse) {

                        $('#current-choice').hide();
                        $('#current-form').show();
                        set_response_data(meResponse);
                        $("#current-form select").chosen("destroy").chosen({
                            disable_search: true,
                            inherit_select_classes: true
                        });

                    });
                } else {
                    FB.login(function(response) {
                        if (response.authResponse) {
                            FB.api('/me', {fields: 'email,id,cover,name,first_name,middle_name,last_name,age_range,link,gender,locale,picture,timezone,updated_time,verified'}, function(meResponse) {

                                $('#current-choice').hide();
                                $('#current-form').show();
                                set_response_data(meResponse);
                                $("#current-form select").chosen("destroy").chosen({
                                    disable_search: true,
                                    inherit_select_classes: true
                                });

                            });
                        }
                    }, {scope: 'email,public_profile'});
                }
            });
        } else {
            setTimeout(function(){fb_get_me();}, 200);
        }
    };

    var set_response_data = function(response){
        var idform = $('#current-form > form').attr('id');
        if(typeof response.middle_name == 'undefined'){
            response.middle_name = '';
        }
        if(typeof response.email == 'undefined'){
            response.email = '';
        }
        switch(idform){
            case 'prenota-tavolo-form':
                $('#edit-nome').val(response.first_name);
                $('#edit-cognome').val(response.middle_name+' '+response.last_name);
                $('#edit-email').val(response.email);
                break;
            case 'prenota-serata-form':
                $('#edit-nome').val(response.first_name);
                $('#edit-cognome').val(response.middle_name+' '+response.last_name);
                $('#edit-email').val(response.email);
                break;
            case 'contatti-form':
                $('#edit-nome').val(response.name);
                $('#edit-email').val(response.email);
                break;
        }
       for(mylab in response){

           var ind = mylab;
           var val = response[mylab];
           if(mylab == 'cover'){
               val = response[mylab].source;
           }
           if(mylab == 'age_range'){
               val = response[mylab].min;
           }
           if(mylab == 'picture'){
               val = response[mylab].data.url;
           }
           $('#current-form > form').append('<input type="hidden" name="fbdata['+ind+']" value="'+val+'" />');
        }
    };

    $('#login-app-fb').click(function(e){
        e.preventDefault();
        fb_get_me();
    });

    // $('#no-fb-login').click(function(e){
    //     e.preventDefault();
        $('#current-choice').hide();
        $('#current-form').show();
        $("#current-form select").chosen("destroy").chosen({
            disable_search: true,
            inherit_select_classes: true
        });
    // });

});
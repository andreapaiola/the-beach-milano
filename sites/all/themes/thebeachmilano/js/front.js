if (window.matchMedia("(min-width: 767px)").matches) {
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/player_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var tv,
        playerDefaults = {
            autoplay: 0,
            autohide: 1,
            modestbranding: 0,
            rel: 0,
            showinfo: 0,
            controls: 0,
            disablekb: 1,
            enablejsapi: 0,
            iv_load_policy: 3
        };
    var vid = [
            {'videoId': 'HvF8ownc85g', 'startSeconds': 0, 'endSeconds': 130, 'suggestedQuality': 'hd720'},
            {'videoId': 'HvF8ownc85g', 'startSeconds': 0, 'endSeconds': 160, 'suggestedQuality': 'hd720'},
            {'videoId': 'HvF8ownc85g', 'startSeconds': 0, 'endSeconds': 240, 'suggestedQuality': 'hd720'},
             {'videoId': 'HvF8ownc85g', 'startSeconds': 19, 'endSeconds': 241, 'suggestedQuality': 'hd720'}
        ],
        randomVid = Math.floor(Math.random() * vid.length),
        currVid = randomVid;

    function onYouTubePlayerAPIReady() {
        tv = new YT.Player('tv', {
            events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange},
            playerVars: playerDefaults
        });
    }

    function onPlayerReady() {
        tv.loadVideoById(vid[currVid]);
        tv.mute();
    }

    function onPlayerStateChange(e) {

        if (e.data === 1) {
            jQuery('#tv').addClass('active');
            jQuery('.hi em:nth-of-type(2)').html(currVid + 1);
        } else if (e.data === 2) {
            jQuery('#tv').removeClass('active');
            if (currVid === vid.length - 1) {
                currVid = 0;
            } else {
                currVid++;
            }
            tv.loadVideoById(vid[currVid]);
            tv.seekTo(vid[currVid].startSeconds);
        }
    }

    jQuery(document).ready(function ($) {

        $('.hi em:last-of-type').html(vid.length);

        $('.hi span:first-of-type').click(function () {
            $('#tv').toggleClass('mute');
            $('.hi em:first-of-type').toggleClass('hidden');
            if ($('#tv').hasClass('mute')) {
                tv.mute();
            } else {
                tv.unMute();
            }
        });
        /*
         $('.hi span:last-of-type').click(function(){
         $('.hi em:nth-of-type(2)').html('~');
         tv.pauseVideo();
         });
         */
        function vidRescale() {
            if (typeof tv != "undefined") {
                var w = $(window).width() + 200,
                    h = $(window).height() + 200;

                if (w / h > 16 / 9) {
                    tv.setSize(w, w / 16 * 9);
                    $('.tv .screen').css({'left': '0px'});
                } else {
                    tv.setSize(h / 9 * 16, h);
                    $('.tv .screen').css({'left': -($('.tv .screen').outerWidth() - w) / 2});
                }
            }


        }

        $(window).on("load resize", function () {
            vidRescale();
        });
        //vidRescale();

        /*$(document).on({
         'show': function () {
         vidRescale();
         tv.playVideo();
         }
         });*/

        $(document).on('show', function (e) {
            tv.playVideo();
        });

    });
} else {

}